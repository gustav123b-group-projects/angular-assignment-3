# NgAssignment3

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.6.

## Install

This project uses node and npm. If you don't have them locally installed please go and set it up. They have step-by-step guides on how to install. Below follows a quick guide:

## Usage

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files. 

1. Landing Page - The user inputs a Trainer name in order to login and be able to store pokemons. 
2. Catalogue Page - The user can choose between several pokemons by pressing the add icon on each pokemon box.
3. Trainer Page - This is where the user has the ability to remove already chosen pokemons by pressing an icon twice.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Contributors

https://gitlab.com/Gustav123b
https://gitlab.com/oskar.nyman

We are two students studying .NET Fullstack at Experis Academy. This project was assigned to us by Noroff School of technology and digital media. Noroff provided the JSON server on Heroku.

We do not claim ownership of the images and audio files used in the project.

## Contributing

Since this is part of a graded assignment no outside contributors was permitted.

## Known Bugs

We have accidentally added the .env files to gitlab. We tried to .ignore those files but realised the damage was already done. We will attempt to rebase after submission on 25.07.2022. 