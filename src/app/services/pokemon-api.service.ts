import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { PokemonData } from "../models/pokemon";
import { Observable } from "rxjs/internal/Observable";
import { retry, catchError } from "rxjs/operators";
import { environment } from "src/environments/environment";
import { trainer } from "../models/trainer";

@Injectable({
    providedIn: "root",
})
export class PokemonApiService {
    apiURL = environment.API_URL;
    apiKEY = environment.API_KEY;

    constructor(private http: HttpClient) {}

    httpOptions = {
        headers: new HttpHeaders({
            "X-API-Key": this.apiKEY,
            "Content-Type": "application/json",
        }),
    };

    getPokemonData(): Observable<PokemonData> {
        return this.http
            .get<PokemonData>(
                "https://pokeapi.co/api/v2/pokemon?offset=0&limit=800"
            )
            .pipe(retry(1));
    }

    getPokemonTrainer(trainerId: number): Observable<PokemonData> {
        return this.http.get<any>(`${this.apiURL}/${trainerId}`).pipe(retry(1));
    }

    // Get additional info about a pokemon
    getPokemonInfo(pokemonId: any, id = true): Observable<any> {
        const url = id
            ? `https://pokeapi.co/api/v2/pokemon/${pokemonId}/`
            : pokemonId;
        return this.http.get<any>(url).pipe(retry(1));
    }

    // Get info about a move
    getMoveInfo(url: string): Observable<any> {
        return this.http.get<any>(url).pipe(retry(1));
    }

    // Update trainer data
    patchPokemonData(trainer: trainer) {
        return this.http
            .put<any>(
                `${this.apiURL}/${trainer.id}`,
                JSON.stringify(trainer),
                this.httpOptions
            )
            .pipe(
                retry(1),
                catchError((error): any => {
                    console.log("Caught in CatchError. Returning 0");
                    console.log(error);
                })
            );
    }
}
