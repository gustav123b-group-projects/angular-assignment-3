import { Injectable } from "@angular/core";
import { battleLogEntry } from "../models/battle";
import { battlePokemon, move } from "../models/pokemon";

@Injectable({
    providedIn: "root",
})
export class BattleService {
    battleLog?: battleLogEntry[];
    battleOver!: boolean;

    constructor() {
        this.battleLog = [];
        this.battleOver = false;
    }

    getRandomMove(moves: move[]) {
        const availableMoves = moves.filter((move) => move.pp > 0);
        const rand = Math.floor(Math.random() * availableMoves.length);
        const move = availableMoves[rand];
        console.log(availableMoves);
        console.log(move);
        return move;
    }
}
