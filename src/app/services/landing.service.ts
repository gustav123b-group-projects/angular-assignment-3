import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { map, Observable, of, switchMap, tap } from "rxjs";
import { environment } from "src/environments/environment";
import { StorageKeys } from "../enums/storage-keys.enum";
import { trainer } from "../models/trainer";
//import { trainer } from "../models/trainer.model";
import { StorageUtil } from "../utils/storage.util";

const { API_URL, API_KEY } = environment;

@Injectable({
    providedIn: "root",
})
export class LandingService {
    // Dependency Injection
    // readonly because it will never be reinstantiated
    constructor(
        private readonly http: HttpClient,
        //private readonly router: Router
    ) {}

    // Models, HttpClient, Observables, and RxJS operators

    // Login
    public landing(username: string): Observable<trainer>{
        
        return this.checkTrainerName(username).pipe(        // pipe means that we can change or do something to the data in a "pipe"/area, e.g tap, switchMap etc.
            switchMap((user: trainer | undefined) => {
                // allows to switch to different observables
                if (user === undefined) {
                    // user does not exist
                    return this.createUser(username);
                }
                else{
                    //this.router.navigateByUrl("/catalogue");
                    return of(user);
                }
                

            })

            /* tap((user: trainer) => {
            StorageUtil.storageSave<trainer>(StorageKeys.trainer, user);
        })  */
        );
    }

    // Check if user exists
    private checkTrainerName(
        username: string
    ): Observable<trainer | undefined> {
        // if there is an empty array returned the undefined will be returned
        return this.http.get<trainer[]>(`${API_URL}?username=${username}`).pipe(
            // RxJS Operators
            map((response: trainer[]) => {
                return response.pop();
            })
        );
    }

    // If not user, create a user
    private createUser(username: string): Observable<trainer> {
        // user
        const trainer = {
            username,
            pokemon: [],
        };

        // headers -> API Key
        const headers = new HttpHeaders({
            "Content-Type": "application/json",
            "x-api-key": API_KEY,
        });

        // POST - Create items on the server
        return this.http.post<trainer>(API_URL, trainer, {
            headers,
        });
    }
}
