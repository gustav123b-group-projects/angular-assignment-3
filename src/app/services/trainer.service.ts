import { Injectable } from "@angular/core";
import { trainer, pokemon } from "../models/trainer";
import { PokemonApiService } from "./pokemon-api.service";
import { Router } from "@angular/router";
import { battlePokemon } from "../models/pokemon";

@Injectable({
    providedIn: "root",
})
export class TrainerService {
    trainer!: trainer;
    battlePokemon?: battlePokemon;
    opponentBattlePokemon?: battlePokemon;

    constructor(
        private restApi: PokemonApiService,
        private readonly router: Router
    ) {}

    updateLocalStorate() {
        localStorage.setItem("trainer", JSON.stringify(this.trainer));
    }

    getLocalStorage(key = "trainer") {
        return localStorage.getItem(key);
    }

    setTrainerData(data: trainer, redirect = false, landing: any) {
        this.restApi.patchPokemonData(data).subscribe((data) => {
            this.trainer = data;
            this.updateLocalStorate();

            // If logged in on mount visit catalogue page instead of log in page
            if (redirect) {
                this.router.navigateByUrl("/catalogue");
                landing.emit();
            }
        });
    }

    addPokemon(pokemon: pokemon, imgUrl: string, types: string[]) {
        const patchedTrainer = {
            ...this.trainer,
            pokemon: [
                ...this.trainer.pokemon,
                {
                    id: pokemon.id,
                    name: pokemon.name,
                    img: imgUrl,
                    types: types,
                },
            ],
        };
        this.update(patchedTrainer);
    }

    // Takes in pokemons index in trainer pokemon array as parameter
    removePokemon(i: number) {
        const pokemons = [...this.trainer.pokemon];
        pokemons.splice(i, 1);

        const patchedTrainer = {
            ...this.trainer,
            pokemon: pokemons,
        };

        this.update(patchedTrainer);
    }

    isPokemonCaptured(pokemon: pokemon): boolean {
        return this.trainer.pokemon.some((p: any) => p.name === pokemon.name);
    }

    getRandomPokemon() {
        const data = JSON.parse(this.getLocalStorage("pokemonData")!);
        const rand = Math.floor(Math.random() * data.results.length);
        const pokemon = data.results[rand];

        return pokemon;
    }

    // false because this function is not called on mount
    update(trainer: trainer) {
        this.setTrainerData(trainer, false, "");
    }
}
