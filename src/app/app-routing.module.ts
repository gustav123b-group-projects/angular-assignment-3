import { Component, NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthenticationComponent } from "./components/authentication/authentication.component";
import { CataloguePageComponent } from "./components/catalogue-page/catalogue-page.component";
import { LandingFormComponent } from "./components/landing-form/landing-form.component";
import { TrainerPageComponent } from "./components/trainer-page/trainer-page.component";
import { BattlePageComponent } from "./components/battle-page/battle-page.component";
import { AuthGuard } from "./guards/auth.guard";

const routes: Routes = [
    {
        path: "", // this route will redirect to another given route if there is nothing in the navigation field
        /* pathMatch: "full",  // empty path and nothing else
        redirectTo: "/landing"  // redirects to a specified existing route */
        component: AuthenticationComponent,
    },
    {
        path: "landing",
        component: LandingFormComponent,
        //canActivate: [ AuthGuard ]
    },
    {
        path: "catalogue",
        component: CataloguePageComponent,
        canActivate: [AuthGuard], // This is a feature that enables us to control what happens if for example a user is not logged in (exists in local storage).
        // ...This will redirect the user to the landing page in that case.
    },
    {
        path: "trainer-page",
        component: TrainerPageComponent,
        canActivate: [AuthGuard],
    },
    {
        path: "battle",
        component: BattlePageComponent,
        canActivate: [AuthGuard],
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes), // imports all the routes into the RouterModule?
    ], // Import a module
    exports: [RouterModule], // Export module and it's features
})
export class AppRoutingModule {}
