import { NgModule } from "@angular/core";
    import { BrowserModule } from "@angular/platform-browser";
    import { HttpClientModule } from "@angular/common/http";
    import { AppRoutingModule } from './app-routing.module';
    import { FormsModule } from '@angular/forms';

import { AppComponent } from "./app.component";
    import { LandingFormComponent } from "./components/landing-form/landing-form.component";
    import { CataloguePageComponent } from "./components/catalogue-page/catalogue-page.component";
    import { PagesComponent } from "./components/pages/pages.component";
    import { PokemonCatalogueComponent } from "./components/pokemon-catalogue/pokemon-catalogue.component";
    import { PokemonCatalogueItemComponent } from "./components/pokemon-catalogue-item/pokemon-catalogue-item.component";
    import { TrainerPageComponent } from './components/trainer-page/trainer-page.component';
    import { PokemonListComponent } from './components/pokemon-list/pokemon-list.component';
    import { PokemonListItemComponent } from './components/pokemon-list-item/pokemon-list-item.component';
    import { NavbarComponent } from './components/navbar/navbar.component';
    import { AuthenticationComponent } from './components/authentication/authentication.component';
    import { InfoModalComponent } from './components/info-modal/info-modal.component';
import { BattlePageComponent } from './components/battle-page/battle-page.component';
import { BattleSideComponent } from './components/battle-side/battle-side.component';

@NgModule({
    declarations: [
        AppComponent,
        CataloguePageComponent,
        PagesComponent,
        PokemonCatalogueComponent,
        PokemonCatalogueItemComponent,
        TrainerPageComponent,
        PokemonListComponent,
        PokemonListItemComponent,
        NavbarComponent,
        LandingFormComponent,
        AuthenticationComponent,
        InfoModalComponent,
        BattlePageComponent,
        BattleSideComponent
    ],
    imports: [BrowserModule, HttpClientModule, AppRoutingModule, FormsModule],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {}
