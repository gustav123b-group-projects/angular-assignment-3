export interface Pokemon {
    name: string;
    url: string;
}

export interface PokemonData {
    count: number;
    next: null | string;
    previous: null | string;
    results: Pokemon[];
}

export interface battlePokemon {
    img: sprites;
    name: string;
    startHp: number;
    currentHp: number;
    moves: move[];
}

export interface move {
    name: string;
    accuracy: number;
    power: number;
    pp: number;
    startpp: number;
    type: string;
}

export interface sprites {
    front: string;
    back: string;
}
