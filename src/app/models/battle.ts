import { battlePokemon, move } from "./pokemon";

export interface battleLogEntry {
    attacker: battlePokemon;
    move: move;
    target: battlePokemon;
    success: boolean;
    over: boolean;
}
