export interface trainer {
    id?: number;
    username: string;
    pokemon: pokemon[];
}

export interface pokemon {
    name: string;
    img?: string;
    id?: string | number;
    url?: string;
    types?: string[];
}

export interface trainerPokemonItem {
    id: number;
    name: string;
}

// This interface was made redundant due to both contributors working on a similar issues on separate branches.
export interface Trainer {
    id?: number;
    username: string;
    pokemon: pokemon[];
}
