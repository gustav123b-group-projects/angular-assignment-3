import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: "app-pages",
    templateUrl: "./pages.component.html",
    styleUrls: ["./pages.component.scss"],
})
export class PagesComponent implements OnInit {
    @Input() loggedIn!: boolean;

    constructor(private readonly router : Router) {}

    /* handleLanding(): void {
        this.router.navigateByUrl("/catalogue")
    } */

    ngOnInit(): void {}
}
