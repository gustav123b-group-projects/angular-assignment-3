import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BattleSideComponent } from './battle-side.component';

describe('BattleSideComponent', () => {
  let component: BattleSideComponent;
  let fixture: ComponentFixture<BattleSideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BattleSideComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BattleSideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
