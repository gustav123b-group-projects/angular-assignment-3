import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { battlePokemon, move } from "src/app/models/pokemon";
import { BattleService } from "src/app/services/battle.service";
import { TrainerService } from "src/app/services/trainer.service";

@Component({
    selector: "app-battle-side",
    templateUrl: "./battle-side.component.html",
    styleUrls: ["./battle-side.component.scss"],
})
export class BattleSideComponent implements OnInit {
    @Input() pokemon!: battlePokemon;
    @Input() target!: battlePokemon;
    @Input() side?: string;
    @Input() attack!: Function;

    constructor(
        protected trainerService: TrainerService,
        protected battleService: BattleService
    ) {}

    moveUsed(move: move) {
        if (
            this.side === "opponent" ||
            move.pp <= 0 ||
            this.battleService.battleOver
        )
            return;

        this.attack(
            this.pokemon,
            move,
            this.target,
            this.battleService.battleLog
        );
    }

    useRandomMove() {}

    ngOnInit(): void {}
}
