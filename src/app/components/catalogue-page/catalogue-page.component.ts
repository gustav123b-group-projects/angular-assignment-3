import { Component, OnInit } from "@angular/core";
import { PokemonData } from "src/app/models/pokemon";
import { PokemonApiService } from "src/app/services/pokemon-api.service";

@Component({
    selector: "app-catalogue-page",
    templateUrl: "./catalogue-page.component.html",
    styleUrls: ["./catalogue-page.component.scss"],
})
export class CataloguePageComponent implements OnInit {
    pokemonData!: PokemonData;

    constructor(private restApi: PokemonApiService) {}

    ngOnInit(): void {
        // GET data from pokemon API if it does not exist in local storage.
        if (localStorage.getItem("pokemonData") === null) {
            this.restApi.getPokemonData().subscribe((data: PokemonData) => {
                this.pokemonData = data;
                localStorage.setItem("pokemonData", JSON.stringify(data));
            });
        } else {
            this.pokemonData = JSON.parse(localStorage.getItem("pokemonData")!);
        }
    }
}
