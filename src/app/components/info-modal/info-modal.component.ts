import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { pokemon } from "src/app/models/trainer";

@Component({
    selector: "app-info-modal",
    templateUrl: "./info-modal.component.html",
    styleUrls: ["./info-modal.component.scss"],
})
export class InfoModalComponent implements OnInit {
    @Input() pokemon?: pokemon;
    @Input() pokemonInfo?: any;
    // Emitter that calls function in parent component
    @Output("closeInfo") closeInfo: EventEmitter<any> = new EventEmitter();
    types?: string[];

    showMoves!: boolean;
    showGames!: boolean;
    showOther!: boolean;

    constructor() {}

    toggleMoves() {
        this.showMoves = !this.showMoves;
    }

    toggleGames() {
        this.showGames = !this.showGames;
    }

    toggleOther() {
        this.showOther = !this.showOther;
    }

    ngOnInit(): void {
        this.types = this.pokemonInfo.types.map((type: any) => type.type.name);
        this.showMoves, this.showOther, (this.showGames = false);
    }
}
