import { Component, OnInit, OnDestroy } from "@angular/core";
import { TrainerService } from "src/app/services/trainer.service";

@Component({
    selector: "app-trainer-page",
    templateUrl: "./trainer-page.component.html",
    styleUrls: ["./trainer-page.component.scss"],
})
export class TrainerPageComponent implements OnInit {
    audio?: HTMLAudioElement | null;
    audioVolume!: number;
    audioIsMuted!: boolean;

    constructor(protected trainerService: TrainerService) {}

    // Mute music
    muteBtn() {
        this.audioIsMuted = !this.audioIsMuted;

        if (this.audioIsMuted) this.audio!.volume = 0;
        else this.audio!.volume = this.audioVolume;
    }

    ngOnInit(): void {
        this.audio = new Audio();
        this.audioVolume = 0.008;
        this.audioIsMuted = false;
        this.audio.src = "../../../assets/pokecenter.mp3";
        this.audio.volume = this.audioVolume;
        this.audio.loop = true;

        this.audio.load();
        this.audio.play();
    }

    // Prevent multiple instances of music to be played at the same time when visiting trainer page.
    ngOnDestroy(): void {
        this.audio?.pause();
        this.audio = null;
    }
}
