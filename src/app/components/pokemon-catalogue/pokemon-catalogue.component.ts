import { Component, Input, OnInit } from "@angular/core";
import { PokemonData } from "src/app/models/pokemon";
import { TrainerService } from "src/app/services/trainer.service";

@Component({
    selector: "app-pokemon-catalogue",
    templateUrl: "./pokemon-catalogue.component.html",
    styleUrls: ["./pokemon-catalogue.component.scss"],
})
export class PokemonCatalogueComponent implements OnInit {
    @Input() pokemonData!: PokemonData;

    constructor(protected trainerService: TrainerService) {}

    ngOnInit(): void {}
}
