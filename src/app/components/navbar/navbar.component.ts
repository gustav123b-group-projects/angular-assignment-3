import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: "app-navbar",
    templateUrl: "./navbar.component.html",
    styleUrls: ["./navbar.component.scss"],
})
export class NavbarComponent implements OnInit {
    activePage!: string;

    constructor(private readonly router: Router) {}

    protected NavRedirect(destination: string) {
        this.activePage = destination;
        this.router.navigateByUrl(`/${destination}`);
    }

    ngOnInit(): void {
        this.activePage = "landing";
    }
}
