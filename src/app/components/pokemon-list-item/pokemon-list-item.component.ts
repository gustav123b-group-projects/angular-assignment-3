import { Component, Input, OnInit } from "@angular/core";
import { TrainerService } from "src/app/services/trainer.service";
import { PokemonApiService } from "src/app/services/pokemon-api.service";
import { PokemonData } from "src/app/models/pokemon";
import { Router } from "@angular/router";
import { BattleService } from "src/app/services/battle.service";

@Component({
    selector: "app-pokemon-list-item",
    templateUrl: "./pokemon-list-item.component.html",
    styleUrls: ["./pokemon-list-item.component.scss"],
})
export class PokemonListItemComponent implements OnInit {
    @Input() pokemon!: any;
    @Input() listId!: number;
    wiggleCount: number;
    backgroundStyle!: string;
    indexFailSafe!: number;

    constructor(
        private trainerService: TrainerService,
        private restApi: PokemonApiService,
        protected battleService: BattleService,
        private readonly router: Router
    ) {
        this.wiggleCount = 0;
    }

    removeBtnClicked() {
        this.trainerService.removePokemon(this.listId);
    }

    battleBtn() {
        this.wiggleCount++;

        if (this.wiggleCount >= 2) this.initBattle();
    }

    initBattle() {
        this.battleService.battleOver = false;
        this.battleService.battleLog = [];

        this.setBattlePokemon(
            `https://pokeapi.co/api/v2/pokemon/${this.pokemon.id}/`,
            "trainer"
        );

        const opponentData = this.trainerService.getRandomPokemon();
        this.setBattlePokemon(opponentData.url, "opponent");

        let interval = setInterval(() => {
            if (
                this.trainerService.battlePokemon !== undefined &&
                this.trainerService.opponentBattlePokemon !== undefined
            ) {
                clearInterval(interval);
                this.router.navigateByUrl("/battle");
            }
        }, 500);
    }

    setBattlePokemon(url: string, side = "trainer") {
        // Get additional info about a pokemon, such as moves and sprites
        this.restApi.getPokemonInfo(url, false).subscribe((data: any) => {
            const callback = (moves: any) => {
                const battlePokemon = {
                    img: {
                        front: data.sprites.front_default,
                        back: data.sprites.back_default,
                    },
                    name: data.name,
                    startHp: 100,
                    currentHp: 100,
                    moves: moves,
                };
                if (side === "trainer")
                    this.trainerService.battlePokemon = battlePokemon;
                else if (side === "opponent")
                    this.trainerService.opponentBattlePokemon = battlePokemon;
            };
            let indexes = [...Array(data.moves.length).keys()];
            this.getValidMoves(data.moves, indexes, [], callback);
        });
    }

    // Recursive function that returns 4 random valid moves
    // A valid move does damage in 1 turn.
    // Moves that only affect stats such as increasing damage or lowering opponent defence is not a valid move.
    getValidMoves(data: any, moveIndexes: any, moves: any, callback: any) {
        let rand = Math.floor(Math.random() * moveIndexes.length);
        let move = data[moveIndexes[rand]].move;

        this.restApi.getMoveInfo(move.url).subscribe((moveData: any) => {
            if (
                moveData.power > 0 &&
                moveData.meta.min_hits === null &&
                moveData.meta.max_hits === null &&
                moveData.meta.min_turns === null &&
                moveData.meta.max_turns === null
            )
                moves.push({
                    name: moveData.name,
                    power: moveData.power,
                    pp: moveData.pp,
                    startpp: moveData.pp,
                    accuracy: moveData.accuracy,
                    type: moveData.type.name,
                });

            // Prevent accidental infinite loop
            this.indexFailSafe++;
            if (this.indexFailSafe > 50) {
                callback(moves);
                return;
            }

            if (moves.length >= 4) {
                callback(moves);
                return;
            } else {
                /* moveIndexes represent the indexes of moves. Whenever the function gets an invalid
            move that index gets removed from moveIndexes. This is to prevent the same invalid
            move to get randomly. */
                moveIndexes.splice(rand, 1);
                this.getValidMoves(data, moveIndexes, moves, callback);
                return;
            }
        });
    }

    ngOnInit(): void {
        // Multiple types gives a gradient as a background while 1 type is a solid background
        this.backgroundStyle =
            this.pokemon.types.length > 1
                ? `linear-gradient(180deg, var(--clr-${this.pokemon.types[0]}) 0%, var(--clr-${this.pokemon.types[0]}) 50%, var(--clr-${this.pokemon.types[1]}) 100%)`
                : `var(--clr-${this.pokemon.types[0]})`;

        this.indexFailSafe = 0;
    }
}
