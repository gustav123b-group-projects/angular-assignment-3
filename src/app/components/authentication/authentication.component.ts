import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  // This component was created due to issues redirecting/routing to the correct component depending on the user status.

  constructor(
    private readonly trainerService: TrainerService,
    private readonly router: Router) { }

  ngOnInit(): void {
      if(this.trainerService.trainer !== undefined){
          this.router.navigateByUrl("/catalogue");
      }
      else
        this.router.navigateByUrl("/landing");
  }

}
