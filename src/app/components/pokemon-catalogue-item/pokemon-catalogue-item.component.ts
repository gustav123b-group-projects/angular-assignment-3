import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { pokemon } from "src/app/models/trainer";
import { PokemonApiService } from "src/app/services/pokemon-api.service";
import { TrainerService } from "src/app/services/trainer.service";
import { InfoModalComponent } from "../info-modal/info-modal.component";

@Component({
    selector: "app-pokemon-catalogue-item",
    templateUrl: "./pokemon-catalogue-item.component.html",
    styleUrls: ["./pokemon-catalogue-item.component.scss"],
})
export class PokemonCatalogueItemComponent implements OnInit {
    @ViewChild(InfoModalComponent) child!: InfoModalComponent;
    @Input() pokemon!: pokemon;
    @Input() collected!: boolean;
    id!: number;
    imgUrl!: string;
    fontSize!: number;
    isActive!: any;

    // Additional information
    pokemonInfo?: any;
    showInfo!: boolean;

    constructor(
        private restApi: PokemonApiService,
        private trainerService: TrainerService
    ) {}

    addBtnClicked(e: any) {
        this.isActive = this.isActive == "unset" ? true : !this.isActive;
        const add = (data: any) => {
            let types = data.types.map(
                (o: { type: { name: any } }) => o.type.name
            );
            this.trainerService.addPokemon(this.pokemon, this.imgUrl, types);
        };
        this.getAdditionalInfo(add);
    }

    getAdditionalInfo(callback: any) {
        this.restApi.getPokemonInfo(this.id).subscribe((data: any) => {
            this.pokemonInfo = data;
            callback(data);
        });
    }

    // Toggle additional info about pokemon
    infoBtn() {
        // If additional data does not exist, get it from pokemon API.
        if (this.pokemonInfo === undefined) {
            const callback = (data: any) => {
                this.showInfo = !this.showInfo;
            };
            this.getAdditionalInfo(callback);
        } else {
            this.showInfo = !this.showInfo;
        }
    }

    toggleInfo() {
        this.showInfo = !this.showInfo;
    }

    closeInfo() {
        this.showInfo = false;
    }

    ngOnInit(): void {
        // id is after index 33, then remove the "/" - https://pokeapi.co/api/v2/pokemon/1/
        this.id = parseInt(this.pokemon.url!.slice(33).replaceAll("/", ""));
        this.imgUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.id}.png`;
        this.pokemon.id = this.id;

        // Scale font size so longer names get smaller font size
        this.fontSize =
            this.pokemon.name.length <= 8
                ? 16
                : (8 / this.pokemon.name.length) * 16;

        this.isActive = "unset";
        this.showInfo = false;
    }
}
