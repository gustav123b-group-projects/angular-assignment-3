import { Component, Input, OnInit } from "@angular/core";
import { trainerPokemonItem } from "src/app/models/trainer";
import { TrainerService } from "src/app/services/trainer.service";

@Component({
    selector: "app-pokemon-list",
    templateUrl: "./pokemon-list.component.html",
    styleUrls: ["./pokemon-list.component.scss"],
})
export class PokemonListComponent implements OnInit {
    boxBgArr: any[];

    constructor(protected trainerService: TrainerService) {
        this.boxBgArr = [
            { bg: "forest", selected: true },
            { bg: "city", selected: false },
            { bg: "pokecenter", selected: false },
            { bg: "desert", selected: false },
            { bg: "volcano", selected: false },
        ];
    }

    btnDeleteAll() {
        const clearedTrainer = {
            ...this.trainerService.trainer,
            pokemon: [],
        };
        this.trainerService.update(clearedTrainer);
    }

    // Change background where collection is displayed
    bgClick(event: Event, bg: string) {
        const clear = (background: string) => {
            const selected = this.boxBgArr.find(
                ({ selected }) => selected === true
            );
            if (selected !== undefined) selected.selected = false;

            const clicked = this.boxBgArr.find(({ bg }) => bg === background);
            clicked.selected = true;

            return this.boxBgArr;
        };

        this.boxBgArr = clear(bg);
    }

    getSelectedBg() {
        return this.boxBgArr.find(({ selected }) => selected === true);
    }

    ngOnInit(): void {}
}
