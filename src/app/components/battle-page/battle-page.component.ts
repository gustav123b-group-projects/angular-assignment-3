import { Component, OnInit, ViewChild } from "@angular/core";
import { TrainerService } from "src/app/services/trainer.service";
import { battlePokemon, move } from "src/app/models/pokemon";
import { battleLogEntry } from "src/app/models/battle";
import { BattleService } from "src/app/services/battle.service";

@Component({
    selector: "app-battle-page",
    templateUrl: "./battle-page.component.html",
    styleUrls: ["./battle-page.component.scss"],
})
export class BattlePageComponent implements OnInit {
    isLoaded: boolean;

    constructor(
        protected readonly trainerService: TrainerService,
        protected battleService: BattleService
    ) {
        this.isLoaded = false;
    }

    // attacker: battlePokemon, move: move, target: battlePokemon
    attack(
        attacker: battlePokemon,
        move: move,
        target: battlePokemon,
        log: battleLogEntry[],
        isTrainer = true
    ) {
        console.log(attacker);
        console.log(move);
        console.log(target);
        // Accuracy, did the move land or miss
        let success = Math.floor(Math.random() * 100) <= move.accuracy;

        // If move was successfull, remove hp from target
        if (success) target.currentHp -= move.power;

        // Remove 1 PP for using move
        move.pp--;

        // Put entry in the battle log
        this.battleService.battleLog = [
            ...log,
            {
                attacker: attacker,
                move: move,
                target: target,
                success: success,
                over: target.currentHp <= 0,
            },
        ];

        // Check if target died
        if (target.currentHp <= 0) {
            this.battleService.battleOver = true;
            return;
        }

        // If trainer was the one attacking it makes it the opponents turn.
        if (isTrainer) {
            const move = this.battleService.getRandomMove(target.moves);
            this.attack(
                target,
                move,
                attacker,
                this.battleService.battleLog,
                false
            );
        }
    }

    ngOnInit(): void {
        if (
            this.trainerService.battlePokemon !== undefined &&
            this.trainerService.opponentBattlePokemon !== undefined
        ) {
            this.isLoaded = true;
        }
    }
}
