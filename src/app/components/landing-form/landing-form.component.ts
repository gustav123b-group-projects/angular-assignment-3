/* import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-form',
  templateUrl: './landing-form.component.html',
  styleUrls: ['./landing-form.component.scss']
})
export class LandingFormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

} */

import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { NgForm } from "@angular/forms";
import { trainer } from "src/app/models/trainer";
import { LandingService } from "src/app/services/landing.service";
import { TrainerService } from "src/app/services/trainer.service";

@Component({
    selector: "app-landing-form",
    templateUrl: "./landing-form.component.html",
    styleUrls: ["./landing-form.component.scss"],
})
export class LandingFormComponent implements OnInit {
    @Output() landing: EventEmitter<void> = new EventEmitter(); // emit events to the parent component that is hosting the landing form

    // Dependency Injection - inject login service
    constructor(
        private readonly landingService: LandingService,
        private trainerService: TrainerService
    ) {}
    
    protected isInValidUsername = false;

    public landingSubmit(landingForm: NgForm): void | boolean{
        // Destructuring
        const { username } = landingForm.value;

        // Returns if name is less than three characters, is called before we save the trainer data.
        if(username.length < 3){
            this.isInValidUsername = true;
            return false;
        }

        this.landingService.landing(username)
        .subscribe({
            next: (user: trainer) => {
                // if it receives something do the following - sets the data of the user input, sets the redirect bool to true and the emitter event
                this.trainerService.setTrainerData(user, true, this.landing);
            },
            error: () => {},
        });
    }

    // AUDIO STUFF
    audio!: HTMLAudioElement;
    audioVolume!: number;
    audioIsMuted!: boolean;

    muteBtn() {
        this.audioIsMuted = !this.audioIsMuted;

        if (this.audioIsMuted) this.audio.volume = 0;
        else this.audio.volume = this.audioVolume;
    }

    ngOnInit(): void {
        this.audio = new Audio();
        this.audioVolume = 0.008;
        this.audioIsMuted = false;

        this.audio.src = "../../../assets/poketheme.mp3";
        this.audio.volume = this.audioVolume;
        this.audio.load();
        this.audio.play();
        this.audio.loop = true;
    }

    ngOnDestroy(): void {
        this.audio.pause();
    }
}
