
// This class was mainly used for the initial login set-up in which the video tutorial was followed. 
    // ... After a merge we decided to opt for local storage instead rendering this class redundant.
        // ... It lets us save data to the session storage as-well as read from the session storage.

export class StorageUtil {

    public static storageSave<T>(key: string, value: T): void{
        sessionStorage.setItem(key, JSON.stringify(value));
    }
    
    public static storageRead<T>(key: string): T | undefined {
        const storedValue = sessionStorage.getItem(key);
        
        try{
            if(storedValue){
                return JSON.parse(storedValue);
            }
            
            return undefined;
        }
        catch(e) {
            sessionStorage.removeItem(key);
            return undefined;
        }
    }
}
