import { Component } from "@angular/core";
import { PokemonApiService } from "./services/pokemon-api.service";
import { TrainerService } from "./services/trainer.service";
import { trainer } from "./models/trainer";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
})
export class AppComponent {
    title = "ng-assignment-3";
    loggedIn: boolean = true;

    constructor(
        public restApi: PokemonApiService,
        public trainerService: TrainerService
    ) {}

    ngOnInit(): void {
        if (this.trainerService.getLocalStorage() !== null) {
            const trainer = JSON.parse(this.trainerService.getLocalStorage()!);

            this.trainerService.trainer = trainer;
            this.trainerService.updateLocalStorate();
        }
    }
}
